from setuptools import find_packages, setup

setup(
    name="dagster_emulab",
    version="0.1",
    packages=['dagster_emulab'],
    install_requires=[
        'lxml',
        'portal-tools @ git+https://gitlab.flux.utah.edu/stoller/portal-tools',
    ],
)
